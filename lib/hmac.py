class HMAC:
    def __init__(self, key, message, hash_f):
        self.key = self.prepare_key(key, hash_f.blocksize)
        self.message = message
        self.hash_f = hash_f
        self.ipad, self.opad = self.prepare_pads(self.key, self.hash_f.blocksize)

    def prepare_key(self, key, blocksize):
        if len(key) > blocksize:
            m = self.hash_f()
            m.update(key)
            key = bytearray(m.digest())
        elif len(key) < blocksize:
            key += b'\x00' * (blocksize - len(key))

        return key

    def prepare_pads(self, key, blocksize):
        i = bytearray(0x36 ^ key[i] for i in range(blocksize))
        o = bytearray(0x5c ^ key[i] for i in range(blocksize))
        return i, o

    def digest(self):
        m1 = self.hash_f()
        m1.update(self.ipad + self.message)

        m2 = self.hash_f()
        m2.update(self.opad + bytes(m1.digest(), 'utf-8'))
        return m2.digest()
