import unittest

from ripemd import ripemd160

class TestRIPEMD(unittest.TestCase):

    def test_empty_string(self):
        m = ripemd160()
        m.update(b'')
        digest = m.digest()

        assert digest == '9c1185a5c5e9fc54612808977ee8f548b2258d31'

    def test_a(self):
        m = ripemd160()
        m.update(b'a')
        digest = m.digest()

        assert digest == '0bdc9d2d256b3ee9daae347be6f4dc835a467ffe'

    def test_abc(self):
        m = ripemd160()
        m.update(b'abc')
        digest = m.digest()

        assert digest == '8eb208f7e05d987a9b044a8e98c6b087f15a0bfc'

    def test_A_Za_z0_9(self):
        m = ripemd160()
        m.update(b'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
        digest = m.digest()

        assert digest == 'b0e20b6e3116640286ed3a87a5713079b21f5189'


if __name__ == '__main__':
    unittest.main()
