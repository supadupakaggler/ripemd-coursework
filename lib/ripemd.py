#! /usr/bin/python
import struct


rol = lambda x, s: ((x << s) | ((x & 0xFFFFFFFF) >> (32 - s))) & 0xFFFFFFFF
toLittleEndian = lambda word: ((word >> 0) & 0xFF) << 24 | ((word >> 8) & 0xFF) << 16 | ((word >> 16) & 0xFF) << 8 | ((word >> 24) & 0xFF) << 0


class ripemd160:
    blocksize = 160
    
    def __init__(self):
        self.message = b''

    def update(self, bytes):
        self.message += bytes

    def digest(self):
        data = self._align_message(self.message)

        data_words = []
        for i in range(int(len(data) / 4)):
            q = 0
            for j in range(4):
                q |= data[i * 4 + j] << j * 8
            data_words.append(q)

        buf = self._init_buf()
        for i in range(0, len(data_words), 16):
            x = data_words[i:i+16]
            buf = self.rounds(buf, x)

        res = ""
        for i in buf:
            res += "{:08x}".format(toLittleEndian(i))

        return res

    def _align_message(self, message):
        l = len(message) * 8

        # Добавление недостающих битов
        padding_len = (56 - len(message)) % 64 or 64
        message = message + b'\x80' + b'\x00' * (padding_len - 1)

        # Добавление длины сообщения
        message = message + (l%2**64).to_bytes(8,byteorder='little')

        return message

    def rounds(self, buf, x):
        F = lambda j, x, y, z: self.get_f(1 + j // 16, 'left')(x, y, z)
        K = lambda j: self.get_constant(1 + j // 16, 'left')
        K1 = lambda j: self.get_constant(1 + j // 16, 'right')
        R = lambda j: self.get_perm(1 + j // 16, 'left')(j % 16)
        R1 = lambda j: self.get_perm(1 + j // 16, 'right')(j % 16)

        A, B, C, D, E = buf
        A1, B1, C1, D1, E1 = buf

        S = [
            11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8,
            7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12,
            11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5,
            11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12,
            9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6
            ]

        S1 = [
            8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6,
            9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11,
            9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5,
            15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8,
            8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11
            ]

        for j in range(80):
            A = rol(A + F(j, B, C, D) + x[R(j)] + K(j), S[j]) + E
            C = rol(C, 10)
            A, B, C, D, E = E, A, B, C, D

            A1 = rol(A1 + F(79 - j, B1, C1, D1) + x[R1(j)] + K1(j), S1[j]) + E1
            C1 = rol(C1, 10)
            A1, B1, C1, D1, E1 = E1, A1, B1, C1, D1

        return [
            (buf[1] + C + D1),
            (buf[2] + D + E1),
            (buf[3] + E + A1),
            (buf[4] + A + B1),
            (buf[0] + B + C1)
        ]

    def _init_buf(self):
        return [
            0x67452301,
            0xefcdab89,
            0x98badcfe,
            0x10325476,
            0xc3d2e1f0,
        ]

    ro = lambda self, i: [7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8,][i]
    pi = lambda self, i: (9 * i + 5) % 16
    def get_perm(self, round, line):
        return {
            ('left', 1): lambda i: i,
            ('left', 2): lambda i: self.ro(i),
            ('left', 3): lambda i: self.ro(self.ro(i)),
            ('left', 4): lambda i: self.ro(self.ro(self.ro(i))),
            ('left', 5): lambda i: self.ro(self.ro(self.ro(self.ro(i)))),

            ('right', 1): lambda i: self.pi(i),
            ('right', 2): lambda i: self.ro(self.pi(i)),
            ('right', 3): lambda i: self.ro(self.ro(self.pi(i))),
            ('right', 4): lambda i: self.ro(self.ro(self.ro(self.pi(i)))),
            ('right', 5): lambda i: self.ro(self.ro(self.ro(self.ro(self.pi(i))))),
        }[line, round]

    f1 = lambda self, x, y, z: (x ^ y ^ z)
    f2 = lambda self, x, y, z: (x & y) | (~x & z)
    f3 = lambda self, x, y, z: ((x | ~y) ^ z)
    f4 = lambda self, x, y, z: (x & z) | (y & ~z)
    f5 = lambda self, x, y, z: (x ^ (y | ~z))
    def get_f(self, round, line):
        return {
            ('left', 1): self.f1,
            ('left', 2): self.f2,
            ('left', 3): self.f3,
            ('left', 4): self.f4,
            ('left', 5): self.f5,

            ('right', 1): self.f5,
            ('right', 2): self.f4,
            ('right', 3): self.f3,
            ('right', 4): self.f2,
            ('right', 5): self.f1,
        }[line, round]

    def get_constant(self, round, line):
        return {
            ('left', 1): 0x00000000,
            ('left', 2): 0x5A827999,
            ('left', 3): 0x6ED9EBA1,
            ('left', 4): 0x8F1BBCDC,
            ('left', 5): 0xA953FD4E,

            ('right', 1): 0x50A28BE6,
            ('right', 2): 0x5C4DD124,
            ('right', 3): 0x6D703EF3,
            ('right', 4): 0x7A6D76E9,
            ('right', 5): 0x00000000
        }[line, round]


if __name__ == '__main__':
    m = ripemd160()
    print(m.digest())
    m.update(b'The quick brown fox jumps over the lazy cog')
    print(m.digest())
