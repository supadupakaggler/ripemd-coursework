### Запуск приложения
1. Установить python3 https://www.python.org/downloads/
2. Установить flask http://flask.pocoo.org/docs/1.0/installation/
3. Выполнить команду `python3 app.py`
4. Приложение будет доступно по адресу: http://127.0.0.1:5000/
