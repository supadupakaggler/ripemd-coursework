import os
from flask import Flask, request, render_template, flash, jsonify

from lib.ripemd import ripemd160
from lib.hmac import HMAC

app = Flask(__name__)
app.secret_key = '123'

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/hashify_files', methods=['POST'])
def hf():
    fs = request.files.get('files[]')
    m = ripemd160()
    if fs:
        for f in fs:
            m.update(f)
    else:
        m.update(b'')
    return jsonify({'hash': m.digest()})

@app.route('/hashify_text', methods=['POST'])
def ht():
    text = request.data
    m = ripemd160()
    m.update(text)
    hash = m.digest()
    return jsonify({'hash': hash})

@app.route('/hashify_files_with_password', methods=['POST'])
def hfwp():
    fs = request.files.get('files[]', [])
    message = b''
    for f in fs:
        message += f

    pas = request.form.get('password', '')
    pas = bytes(pas, "utf8")

    m = HMAC(pas, message, ripemd160)
    return jsonify({'hash': m.digest()})


if __name__ == '__main__':
    app.run()
